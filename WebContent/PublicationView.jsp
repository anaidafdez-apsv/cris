<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<%@ include file="Header.jsp"%>
<title>Publication</title>

</head>
<body>

	<table>
		<tr>
			<th>ID</td>
			<th>Title</td>
			<th>Publication Name</td>
			<th>Publication Date</td>
			<th>Authors</td>
			<th><a href="UpdateCitationsAPIServlet?id=${ri.id}">Cite Count</a></td>
		<tr>
			<td>${ri.id}</a></td>
			<td>${ri.title}</td>
			<td>${ri.publicationName}</td>
			<td>${ri.publicationDate}</td>
			<td>${ri.authors}</td>
			<td>${ri.citeCount}</td>
		</tr>
	</table>


</body>
</html>