<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<%@ include file="Header.jsp"%>
<title>Admin</title>

</head>
<body>
	<h1>Create Researcher</h1>
	<form action="CreateResearcherServlet" method="post">
		<input type="text" name="id" placeholder="User Id"> <input
			type="text" name="name" placeholder="Name"> <input
			type="text" name="lastname" placeholder="Last name"> <input
			type="text" name="email" placeholder="Email">
		<button type="submit">Create researcher</button>
	</form>
	<h1>Create Publication</h1>
	<form action="CreatePublicationServlet" method="post">
		<input type="text" name="id" placeholder="Publication Id"> <input
			type="text" name="title" placeholder="Title"> <input
			type="text" name="publicationName" placeholder="Publication Name"> <input
			type="text" name="publicationDate" placeholder="Publication Date"> <input
			type="text" name="authors" placeholder="Authors">
		<button type="submit">Create publication</button>
	</form>

</body>
</html>